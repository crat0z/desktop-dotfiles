Packages:

vim (base16)     
ranger (w3m dependency)     
bspwm (dmenu/rofi)     
sxhkd           
compton              
zsh (oh my zsh)          
nitrogen          
lightscreen         
xsetroot           
arc GTK/Firefox          
xdg-user-dirs (removing desktop folder) 
xkb (setting keyboard languages)
