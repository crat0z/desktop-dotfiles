let base16colorspace=256  " Access colors present in 256 colorspace
set background=dark
set t_Co=256
syntax on
colorscheme base16-google
